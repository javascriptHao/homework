// BÀI 1
document.querySelector("#v-pills-bai1 button").onclick = function () {
  var arr = document.querySelectorAll("#v-pills-bai1 input");
  var array = [];
  var result = "";
  for (var i = 0; i < arr.length; i++) {
    array[i] = Number(arr[i].value);
  }
  array.sort((a, b) => a - b);
  for (var j = 0; j < array.length - 1; j++) {
    result += array[j] + " , ";
  }
  result += array[array.length - 1];
  document.getElementById("resultBT1").innerText = result;
};

// BÀI 2
document.querySelector("#v-pills-bai2 select").onchange = function () {
  var str = "";
  var x = document.querySelector("#v-pills-bai2 select").value;
  switch (x) {
    case "B":
      str = "Xin chào Bố!";
      break;
    case "M":
      str = "Xin chào Mẹ!";
      break;
    case "A":
      str = "Xin chào Anh trai!";
      break;
    case "E":
      str = "Xin chào Em gái!";
      break;
    default:
      break;
  }
  document.getElementById("resultBT2").innerText = str;
};

// BÀI 3
document.querySelector("#v-pills-bai3 button").onclick = function () {
  var arr = document.querySelectorAll("#v-pills-bai3 input");
  var array = [];
  var soChan = 0;
  var soLe = 0;
  for (var i = 0; i < arr.length; i++) {
    array[i] = Number(arr[i].value);
  }
  for (var j = 0; j < array.length; j++) {
    if (array[j] % 2 == 0) {
      soChan++;
    } else {
      soLe++;
    }
  }
  document.getElementById(
    "resultBT3"
  ).innerHTML = `Có <span style="color: red">${soChan}</span>  số chẵn và <span style="color: red">${soLe}</span> số lẻ`;
};

// BÀI 4
document.querySelector("#v-pills-bai4 button").onclick = function () {
  var array = document.querySelectorAll("#v-pills-bai4 input");
  var a = Number(array[0].value);
  var b = Number(array[1].value);
  var c = Number(array[2].value);
  var result = "";
  if (a + b > c && a + c > b && b + c > a && a > 0 && b > 0 && c > 0) {
    if (a == b && b == c) {
      result = "Tam giác đều!";
    } else if (a == b || a == c || b == c) {
      result = "Tam giác cân!";
    } else if (
      a == Math.sqrt(Math.pow(b, 2) + Math.pow(c, 2)) ||
      b == Math.sqrt(Math.pow(a, 2) + Math.pow(c, 2)) ||
      c == Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2))
    ) {
      result = "Tam giác vuông!";
    } else {
      result = "Tam giác thường!";
    }
  } else {
    result = "3 cạnh của tam giác không thỏa mãn điều kiện lập thành tam giác!";
  }
  document.getElementById("resultBT4").innerText = result;
};
